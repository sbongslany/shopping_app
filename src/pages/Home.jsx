import React from 'react'
import Announcement from '../compoments/Announcement'
import Navbar from '../compoments/Navbar'

const Home = () => {
    return (
        <div>
            <Announcement />
            <Navbar />
        </div>
    )
}

export default Home
